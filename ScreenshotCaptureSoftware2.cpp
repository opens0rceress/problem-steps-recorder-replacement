/*
*	Button Helper v2 - User action and screenshot capture 
*	Author: Christine Sutton
*	Created: February 2019
*
*	(c) Copyright by Alex's PC Solutions, Macon, GA 
*	    www.alexspc.com
*
*	Program Details: This software does what the Windows Problem Steps Recorder does in a more lightwight capacity. It captures
*	screenshots upon a variety of keyboard and mouse triggers while using any program and cycles those screenshots in memory (HIPAA Compliance) 
*	until prompted. Each screenshot contains a text readout of what action the user took and in what window. The program then utilizes ffmpeg 
*	to compile the screenshots into a video for review and deletes the data. It is a module for a larger program that I have omitted details for 
*	in this note version, as I am under an NDA for it at this time. 
*
*/

#include "stdafx.h"			//Remove when compiling on anything but VS2015
#include "windows.h"
#include "Windowsx.h"
#include "Screenshot.h"
#include <tchar.h>
#include <iostream>
#include <string>
#include <gdiplus.h>
#include <queue>
#include <vector>
#pragma comment(lib, "gdiplus.lib")

using namespace std;
using namespace Gdiplus;

HHOOK hookEnter;							// hook for Enter event
HHOOK hookMouse;							// hook for Mouse events
HHOOK hookButton;							// hook for Button press
HHOOK hookTab;								// hook for Tab event
HHOOK hookLeftArrow;						// hook for Left Arrow event
HHOOK hookRightArrow;						// hook for Right Arrow event
HHOOK hookUpArrow;							// hook for Up Arrow event
HHOOK hookDownArrow;						// hook for Down Arrow event
HHOOK hookHome;								// hook for Home event
HHOOK hookEnd;								// hook for End event
HHOOK hookPgUp;								// hook for Page Up event
HHOOK hookPgDown;							// hook for Page Down event 
KBDLLHOOKSTRUCT kbdStructEnter;				// Callback function data for each event
KBDLLHOOKSTRUCT kbdStructMouse;
KBDLLHOOKSTRUCT kbdStructTab;
KBDLLHOOKSTRUCT kbdStructLeftArrow;
KBDLLHOOKSTRUCT kbdStructRightArrow;
KBDLLHOOKSTRUCT kbdStructUpArrow;
KBDLLHOOKSTRUCT kbdStructDownArrow;
KBDLLHOOKSTRUCT kbdStructHome;
KBDLLHOOKSTRUCT kbdStructEnd;
KBDLLHOOKSTRUCT kbdStructPgUp;
KBDLLHOOKSTRUCT kbdStructPgDown;
static const int MAX_SCREENSHOTS = 15;		// Max screenshots allowed in memory 
queue<Screenshot> screenshotQueue;			// Queue of screenshots
bool isSecondClick = false;					// Double click tracking
long long trackClickTime = 0;				// Double click tracking
vector<string> configPaths (6);				// Store file paths in installation config file

void DumpScreenshotData();
string GetActiveWindowTitle();
void ManageQueue(Screenshot& ssObj);
bool keydown(int key);


// HookCallbackMouse(): Creates a hook that catches all possible mouse clicks anywhere on the screen,
// then captures a screenshot and adds it to the screenshot queue
LRESULT __stdcall HookCallbackMouse(int nCode, WPARAM wParam, LPARAM lParam)
{
	Screenshot ssObj;		//New screenshot pointer
	string window;			//Title of current window
	string action;			//Which action the user took
	POINT pt;				//For capturing mouse location

	if (nCode >= 0)
	{
		if (wParam == WM_LBUTTONDOWN)
		{
			long long lastMsgTime = trackClickTime;
			trackClickTime = GetTickCount();
			GetCursorPos(&pt);
			if ((trackClickTime - lastMsgTime) < GetDoubleClickTime())
			{
				//Double click
				kbdStructMouse = *((KBDLLHOOKSTRUCT*)lParam);
				window = GetActiveWindowTitle();
				action = "double clicked";
				if (window == "")
				{
					window = "no Active Window";
				}

				ssObj.TakeScreenshot(action, window, pt.x, pt.y);
				ManageQueue(ssObj);
			}
			/*else if (DragDetect(hwnd, pt))
			{
			//Mouse drag
			kbdStructMouse = *((KBDLLHOOKSTRUCT*)lParam);
			window = GetActiveWindowTitle();
			action = "dragged mouse";
			if (window == "")
			{
			window = "no Active Window";
			}
			ssObj.TakeScreenshot(action, window);
			ManageQueue(ssObj);
			} */
			else
			{
				//Single left click
				kbdStructMouse = *((KBDLLHOOKSTRUCT*)lParam);
				window = GetActiveWindowTitle();
				action = "left clicked";
				if (window == "")
				{
					window = "no Active Window";
				}
				ssObj.TakeScreenshot(action, window, pt.x, pt.y);
				ManageQueue(ssObj);
			}
	
		}
		else if (wParam == WM_RBUTTONDOWN)
		{
			//Right click
			GetCursorPos(&pt);
			kbdStructMouse = *((KBDLLHOOKSTRUCT*)lParam);
			window = GetActiveWindowTitle();
			action = "right clicked";
			if (window == "")
			{
				window = "no Active Window";
			}
			ssObj.TakeScreenshot(action, window, pt.x, pt.y);
			ManageQueue(ssObj);
		}
		else if (wParam == WM_MBUTTONDOWN)
		{
			//Middle mouse click
			GetCursorPos(&pt);
			kbdStructMouse = *((KBDLLHOOKSTRUCT*)lParam);
			window = GetActiveWindowTitle();
			action = "middle clicked";
			if (window == "")
			{
				window = "no Active Window";
			}
			ssObj.TakeScreenshot(action, window, pt.x, pt.y);
			ManageQueue(ssObj);
		}
		else if (wParam == WM_MOUSEWHEEL)
		{
			//Mouse scroll
			GetCursorPos(&pt);
			kbdStructMouse = *((KBDLLHOOKSTRUCT*)lParam);
			window = GetActiveWindowTitle();
			action = "mouse scrolled";
			if (window == "")
			{
				window = "no Active Window";
			}
			ssObj.TakeScreenshot(action, window, pt.x, pt.y);
			ManageQueue(ssObj);
		}
	}

	return CallNextHookEx(hookMouse, nCode, wParam, lParam);
}

// HookCallbackEnter(): Creates a hook that catches enter key presses anywhere on the screen, 
// then captures a screenshot and adds it to the screenshot queue
LRESULT __stdcall HookCallbackEnter(int nCode, WPARAM wParam, LPARAM lParam)
{
	Screenshot ssObj;
	string action = "pressed Enter";
	string window;
	POINT pt;

	if (nCode >= 0)
	{
		if (wParam == WM_KEYDOWN)
		{
			GetCursorPos(&pt);
			kbdStructEnter = *((KBDLLHOOKSTRUCT*)lParam);
			if (kbdStructEnter.vkCode == VK_RETURN)
			{
				window = GetActiveWindowTitle();
				ssObj.TakeScreenshot(action, window, pt.x, pt.y);
				ManageQueue(ssObj);
			}
		}
	}

	return CallNextHookEx(hookEnter, nCode, wParam, lParam);
}
// HookCallbackTab(): Creates a hook that catches tab key presses anywhere on the screen, 
// then captures a screenshot and adds it to the screenshot queue
LRESULT __stdcall HookCallbackTab(int nCode, WPARAM wParam, LPARAM lParam)
{
	Screenshot ssObj;
	string action = "pressed Tab";
	string window;
	POINT pt;

	if (nCode >= 0)
	{
		if (wParam == WM_KEYDOWN)
		{
			GetCursorPos(&pt);
			kbdStructTab = *((KBDLLHOOKSTRUCT*)lParam);
			if (kbdStructTab.vkCode == VK_TAB)
			{
				window = GetActiveWindowTitle();
				ssObj.TakeScreenshot(action, window, pt.x, pt.y);
				ManageQueue(ssObj);
			}
		}
	}

	return CallNextHookEx(hookTab, nCode, wParam, lParam);
}

// HookCallbackPgUp(): Creates a hook that catches Page Up key presses anywhere on the screen, 
// then captures a screenshot and adds it to the screenshot queue
LRESULT __stdcall HookCallbackPgUp(int nCode, WPARAM wParam, LPARAM lParam)
{
	Screenshot ssObj;
	string action = "pressed Page Up";
	string window;
	POINT pt;

	if (nCode >= 0)
	{
		if (wParam == WM_KEYDOWN)
		{
			GetCursorPos(&pt);
			kbdStructPgUp = *((KBDLLHOOKSTRUCT*)lParam);
			if (kbdStructPgUp.vkCode == VK_PRIOR)
			{
				window = GetActiveWindowTitle();
				ssObj.TakeScreenshot(action, window, pt.x, pt.y);
				ManageQueue(ssObj);
			}
		}
	}

	return CallNextHookEx(hookPgUp, nCode, wParam, lParam);
}

// HookCallbackPgDown(): Creates a hook that catches Page Down key presses anywhere on the screen, 
// then captures a screenshot and adds it to the screenshot queue
LRESULT __stdcall HookCallbackPgDown(int nCode, WPARAM wParam, LPARAM lParam)
{
	Screenshot ssObj;
	string action = "pressed Page Down";
	string window;
	POINT pt;

	if (nCode >= 0)
	{
		if (wParam == WM_KEYDOWN)
		{
			GetCursorPos(&pt);
			kbdStructPgDown = *((KBDLLHOOKSTRUCT*)lParam);
			if (kbdStructPgDown.vkCode == VK_NEXT)
			{
				window = GetActiveWindowTitle();
				ssObj.TakeScreenshot(action, window, pt.x, pt.y);
				ManageQueue(ssObj);
			}
		}
	}

	return CallNextHookEx(hookPgDown, nCode, wParam, lParam);
}

// HookCallbackHome(): Creates a hook that catches Home key presses anywhere on the screen, 
// then captures a screenshot and adds it to the screenshot queue
LRESULT __stdcall HookCallbackHome(int nCode, WPARAM wParam, LPARAM lParam)
{
	Screenshot ssObj;
	string action = "pressed Home";
	string window;
	POINT pt;

	if (nCode >= 0)
	{
		if (wParam == WM_KEYDOWN)
		{
			GetCursorPos(&pt);
			kbdStructHome = *((KBDLLHOOKSTRUCT*)lParam);
			if (kbdStructHome.vkCode == VK_HOME)
			{
				window = GetActiveWindowTitle();
				ssObj.TakeScreenshot(action, window, pt.x, pt.y);
				ManageQueue(ssObj);
			}
		}
	}

	return CallNextHookEx(hookHome, nCode, wParam, lParam);
}

// HookCallbackEnd(): Creates a hook that catches End key presses anywhere on the screen, 
// then captures a screenshot and adds it to the screenshot queue
LRESULT __stdcall HookCallbackEnd(int nCode, WPARAM wParam, LPARAM lParam)
{
	Screenshot ssObj;
	string action = "pressed End";
	string window;
	POINT pt;

	if (nCode >= 0)
	{
		if (wParam == WM_KEYDOWN)
		{
			GetCursorPos(&pt);
			kbdStructEnd = *((KBDLLHOOKSTRUCT*)lParam);
			if (kbdStructEnd.vkCode == VK_END)
			{
				window = GetActiveWindowTitle();
				ssObj.TakeScreenshot(action, window, pt.x, pt.y);
				ManageQueue(ssObj);
			}
		}
	}

	return CallNextHookEx(hookEnd, nCode, wParam, lParam);
}

// HookCallbackButton(): Creates a hook that catches the button press, ctrl-shift-i 
// This causes the main APC button software to launch, then dumps the screenshots in memory
// to a temp folder and compiles them into a movie using ffmpeg. The movie is moved to the 
// output folder, the temp files are deleted, and the program waits for the next hook. 
LRESULT __stdcall HookCallbackButton(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode >= 0)
	{
		if (wParam == WM_KEYDOWN)
		{

			if (keydown(VK_CONTROL) && keydown(VK_SHIFT) && keydown(VK_MENU) && keydown(0x48))
			{
				//Start the button software
				STARTUPINFO info = { sizeof(info) };
				PROCESS_INFORMATION pi;
				const char *path = configPaths[0].c_str();
				if (CreateProcess(path, NULL, NULL, NULL, TRUE, 0, NULL, NULL, &info, &pi)) { }
				else
				{
					MessageBox(NULL, "Unable to open APC Button launcher", _T("Error"), MB_ICONERROR);
					exit(0);
				}

				//Grab %TEMP% directory address for current user
				DWORD const bufferSize = ::GetTempPath(0u, nullptr) + 1u; 
				wchar_t* buffer = new wchar_t[bufferSize];
				memset(buffer, 0x00, bufferSize);

				//Write screenshots to temp
				DumpScreenshotData();
				
				//Set working directory
				const char *curdir = configPaths[4].c_str();
				SetCurrentDirectory(curdir);

				//Start ffmpeg and wait for it to complete
				STARTUPINFO info2 = { sizeof(info2) };
				PROCESS_INFORMATION pi2;
				const char *path2 = configPaths[1].c_str();
				string cmdstr = "-y -r 1/2 -start_number 0 -i %3d.jpg -c:v libx264 -vf \"fps = 5, format = yuv420p, pad = ceil(iw / 2) * 2:ceil(ih / 2) * 2\" \"" + configPaths[2] + "\"";
				char* cmd = new char[cmdstr.length() + 1];
				strcpy(cmd, cmdstr.c_str());
				
				if (CreateProcess(path2, cmd, NULL, NULL, TRUE, 0, NULL, NULL, &info2, &pi2))
				{
					WaitForSingleObject(pi2.hProcess, INFINITE);
					CloseHandle(pi2.hProcess);
					CloseHandle(pi2.hThread);
				}
				else
				{
					MessageBox(NULL, "Unable to open ffmpeg", _T("Error"), MB_ICONERROR);
					exit(0);
				}

				//Move video to install folder location
				const char *startPath = configPaths[2].c_str();
				const char *destPath = configPaths[3].c_str();
				try {
					if (MoveFile(startPath, destPath)) { }
					else
					{
						throw("Unable to move output mpeg from Temp to output folder");
					}
				}
				catch (exception e) {
					MessageBox(NULL, e.what(), _T("Error"), MB_ICONERROR);
				}

				//Delete images in temp
				const char* folderPath = configPaths[4].c_str();
				char fileFound[256];
				WIN32_FIND_DATA info3;
				HANDLE hp;
				sprintf(fileFound, "%s\\*.*", folderPath);
				hp = FindFirstFile(fileFound, &info3);
				do
				{
					sprintf(fileFound, "%s\\%s", folderPath, info3.cFileName);
					DeleteFile(fileFound);

				} while (FindNextFile(hp, &info3));
				FindClose(hp);
				
				//Manage memory
				delete[] buffer;
				delete[] cmd;
			}
		}
	}
	return CallNextHookEx(hookButton, nCode, wParam, lParam);
}
// HookCallbackLeftArrow(): Creates a hook that catches left arrow key presses anywhere on the screen, 
// then captures a screenshot and adds it to the screenshot queue
LRESULT __stdcall HookCallbackLeftArrow(int nCode, WPARAM wParam, LPARAM lParam)
{
	Screenshot ssObj;
	string action = "pressed Left Arrow";
	string window;
	POINT pt;

	if (nCode >= 0)
	{
		if (wParam == WM_KEYDOWN)
		{
			GetCursorPos(&pt);
			kbdStructLeftArrow = *((KBDLLHOOKSTRUCT*)lParam);
			if (kbdStructLeftArrow.vkCode == VK_LEFT)
			{
				window = GetActiveWindowTitle();
				ssObj.TakeScreenshot(action, window, pt.x, pt.y);
				ManageQueue(ssObj);
			}
		}
	}

	return CallNextHookEx(hookLeftArrow, nCode, wParam, lParam);
}

// HookCallbackRightArrow(): Creates a hook that catches right arrow key presses anywhere on the screen, 
// then captures a screenshot and adds it to the screenshot queue
LRESULT __stdcall HookCallbackRightArrow(int nCode, WPARAM wParam, LPARAM lParam)
{
	Screenshot ssObj;
	string action = "pressed Right Arrow";
	string window;
	POINT pt;

	if (nCode >= 0)
	{
		if (wParam == WM_KEYDOWN)
		{
			GetCursorPos(&pt);
			kbdStructRightArrow = *((KBDLLHOOKSTRUCT*)lParam);
			if (kbdStructRightArrow.vkCode == VK_RIGHT)
			{
				window = GetActiveWindowTitle();
				ssObj.TakeScreenshot(action, window, pt.x, pt.y);
				ManageQueue(ssObj);
			}
		}
	}

	return CallNextHookEx(hookRightArrow, nCode, wParam, lParam);
}

// HookCallbackUpArrow(): Creates a hook that catches up arrow key presses anywhere on the screen, 
// then captures a screenshot and adds it to the screenshot queue
LRESULT __stdcall HookCallbackUpArrow(int nCode, WPARAM wParam, LPARAM lParam)
{
	Screenshot ssObj;
	string action = "pressed Up Arrow";
	string window;
	POINT pt;

	if (nCode >= 0)
	{
		if (wParam == WM_KEYDOWN)
		{
			GetCursorPos(&pt);
			kbdStructUpArrow = *((KBDLLHOOKSTRUCT*)lParam);
			if (kbdStructUpArrow.vkCode == VK_UP)
			{
				window = GetActiveWindowTitle();
				ssObj.TakeScreenshot(action, window, pt.x, pt.y);
				ManageQueue(ssObj);
			}
		}
	}

	return CallNextHookEx(hookUpArrow, nCode, wParam, lParam);
}

// HookCallbackDownArrow(): Creates a hook that catches down arrow key presses anywhere on the screen, 
// then captures a screenshot and adds it to the screenshot queue
LRESULT __stdcall HookCallbackDownArrow(int nCode, WPARAM wParam, LPARAM lParam)
{
	Screenshot ssObj;
	string action = "pressed Down Arrow";
	string window;
	POINT pt;

	if (nCode >= 0)
	{
		if (wParam == WM_KEYDOWN)
		{
			GetCursorPos(&pt);
			kbdStructDownArrow = *((KBDLLHOOKSTRUCT*)lParam);
			if (kbdStructDownArrow.vkCode == VK_DOWN)
			{
				window = GetActiveWindowTitle();
				ssObj.TakeScreenshot(action, window, pt.x, pt.y);
				ManageQueue(ssObj);
			}
		}
	}

	return CallNextHookEx(hookDownArrow, nCode, wParam, lParam);
}

/***********************************************************

Set Hooks - Functions to set hooks for all events in main

************************************************************/
void SetHookEnter()
{
	if (!(hookEnter = SetWindowsHookEx(WH_KEYBOARD_LL, HookCallbackEnter, NULL, 0)))
	{
		MessageBox(NULL, _T("Failed to install hook!"), _T("Error"), MB_ICONERROR);
	}
}

void SetHookMouse()
{
	if (!(hookMouse = SetWindowsHookEx(WH_MOUSE_LL, HookCallbackMouse, NULL, 0)))
	{
		MessageBox(NULL, _T("Failed to install hook!"), _T("Error"), MB_ICONERROR);
	}
}

void SetHookButton()
{
	if (!(hookButton = SetWindowsHookEx(WH_KEYBOARD_LL, HookCallbackButton, NULL, 0)))
	{
		MessageBox(NULL, _T("Failed to install hook!"), _T("Error"), MB_ICONERROR);
	}
}

void SetHookTab()
{
	if (!(hookTab = SetWindowsHookEx(WH_KEYBOARD_LL, HookCallbackTab, NULL, 0)))
	{
		MessageBox(NULL, _T("Failed to install hook!"), _T("Error"), MB_ICONERROR);
	}
}

void SetHookPgUp()
{
	if (!(hookEnter = SetWindowsHookEx(WH_KEYBOARD_LL, HookCallbackPgUp, NULL, 0)))
	{
		MessageBox(NULL, _T("Failed to install hook!"), _T("Error"), MB_ICONERROR);
	}
}

void SetHookPgDown()
{
	if (!(hookEnter = SetWindowsHookEx(WH_KEYBOARD_LL, HookCallbackPgDown, NULL, 0)))
	{
		MessageBox(NULL, _T("Failed to install hook!"), _T("Error"), MB_ICONERROR);
	}
}

void SetHookHome()
{
	if (!(hookHome = SetWindowsHookEx(WH_KEYBOARD_LL, HookCallbackHome, NULL, 0)))
	{
		MessageBox(NULL, _T("Failed to install hook!"), _T("Error"), MB_ICONERROR);
	}
}

void SetHookEnd()
{
	if (!(hookEnd = SetWindowsHookEx(WH_KEYBOARD_LL, HookCallbackEnd, NULL, 0)))
	{
		MessageBox(NULL, _T("Failed to install hook!"), _T("Error"), MB_ICONERROR);
	}
}

void SetHookLeftArrow()
{
	if (!(hookLeftArrow = SetWindowsHookEx(WH_KEYBOARD_LL, HookCallbackLeftArrow, NULL, 0)))
	{
		MessageBox(NULL, _T("Failed to install hook!"), _T("Error"), MB_ICONERROR);
	}
}

void SetHookRightArrow()
{

	if (!(hookRightArrow = SetWindowsHookEx(WH_KEYBOARD_LL, HookCallbackRightArrow, NULL, 0)))
	{
		MessageBox(NULL, _T("Failed to install hook!"), _T("Error"), MB_ICONERROR);
	}
}

void SetHookUpArrow()
{

	if (!(hookUpArrow = SetWindowsHookEx(WH_KEYBOARD_LL, HookCallbackUpArrow, NULL, 0)))
	{
		MessageBox(NULL, _T("Failed to install hook!"), _T("Error"), MB_ICONERROR);
	}
}

void SetHookDownArrow()
{

	if (!(hookDownArrow = SetWindowsHookEx(WH_KEYBOARD_LL, HookCallbackDownArrow, NULL, 0)))
	{
		MessageBox(NULL, _T("Failed to install hook!"), _T("Error"), MB_ICONERROR);
	}
}

/***********************************************************

Hook Releases - In case you need to let 'em off the hook easy

************************************************************/

void ReleaseHookMouse()
{
	UnhookWindowsHookEx(hookMouse);
}

void ReleaseHookEnter()
{
	UnhookWindowsHookEx(hookEnter);
}

void ReleaseHookTab()
{
	UnhookWindowsHookEx(hookTab);
}

void ReleaseHookPgUp()
{
	UnhookWindowsHookEx(hookPgUp);
}

void ReleaseHookPgDown()
{
	UnhookWindowsHookEx(hookPgDown);
}


void ReleaseHookHome()
{
	UnhookWindowsHookEx(hookHome);
}

void ReleaseHookEnd()
{
	UnhookWindowsHookEx(hookEnd);
}

void ReleaseHookUpArrow()
{
	UnhookWindowsHookEx(hookUpArrow);
}

void ReleaseHookLeftArrow()
{
	UnhookWindowsHookEx(hookLeftArrow);
}

void ReleaseHookRightArrow()
{
	UnhookWindowsHookEx(hookRightArrow);
}

void ReleaseHookDownArrow()
{
	UnhookWindowsHookEx(hookDownArrow);
}

/************************************************************

General Functions

*************************************************************/

//GetActiveWindowTitle(): Reads the title of the user's active window and returns the value
string GetActiveWindowTitle()
{
	char wnd_title[64];
	HWND hwnd = GetForegroundWindow(); 
	GetWindowTextA(hwnd, wnd_title, sizeof(wnd_title));
	return wnd_title;
}

//keydown(): Helps detect multikey hook in HookCallbackButton()
bool keydown(int key)
{
	return (GetAsyncKeyState(key) & 0x8000) != 0;
}

//ManageQueue(): Handles the addition of a new Screenshot object to the queue
void ManageQueue(Screenshot& ssObj)
{
	//if queue contains max screenshots, pop off first element and push new object
	if (screenshotQueue.size() == MAX_SCREENSHOTS)
	{
		screenshotQueue.front().memoryManagement();
		screenshotQueue.pop();
		screenshotQueue.push(ssObj);
	}
	else
	{
		//else just push new object into queue
		screenshotQueue.push(ssObj);
	}
}

//DumpScreenshotData(): When the button is pushed, dumps screenshots in memory to temp folder in button install
void DumpScreenshotData()
{
	string filename;
	int size = screenshotQueue.size();
	while (!screenshotQueue.empty())
	{
		for (int i = 0; i < size; i++)
		{
			filename = "0";

			if (i < 10)
			{
				filename = "00";
			}

			filename = filename + to_string(i + 1) + ".jpg";
			screenshotQueue.front().SaveScreenshot(filename);
			screenshotQueue.pop();
		}
	}
}

//ConfigInit(): Reads in a config file generated at installation that provides locations of key EXEs and folders
void ConfigInit()
{
	string line;
	int lineCounter = 0;
	ifstream readInFile;
	readInFile.open("config.txt");

	if (readInFile.is_open())
	{
		while (getline(readInFile, line)) {
			configPaths[lineCounter] = line;
			lineCounter++;
		}
	}
	else
	{
		MessageBox(NULL, "Unable to open install config file", _T("Error"), MB_ICONERROR);
		exit(0);
	}

	readInFile.close();
}

int main()
{
	//Read config file with installer data
	ConfigInit();

	//Set hooks for each event
	SetHookEnter();
	SetHookMouse();
	SetHookButton();
	SetHookTab();
	SetHookPgUp();
	SetHookPgDown();
	SetHookHome();
	SetHookEnd();
	SetHookLeftArrow();
	SetHookRightArrow();
	SetHookUpArrow();
	SetHookDownArrow();

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{

	} 
}