#include "stdafx.h"
#include <gdiplus.h>
#include <iostream>
#include <fstream>
#include <string>
#include "windows.h"
#pragma once
#pragma comment(lib, "gdiplus.lib")


using namespace std;
using namespace Gdiplus;


class Screenshot
{
private:
	HDC dc, memdc, fontdc;
	HBITMAP membit;
	Bitmap* bmpPtr;
	CLSID clsid;
	ULONG_PTR gdiplusToken;
	int GetEncoderClsid(const WCHAR* format, CLSID* pClsid);

public:
	Screenshot();
	~Screenshot();
	void TakeScreenshot(string userAction, string winName, long xMousePos, long yMousePos);
	void SaveScreenshot(string filename);
	void memoryManagement();
};
