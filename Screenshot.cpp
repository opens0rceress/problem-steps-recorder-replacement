#include "stdafx.h"		//Remove when compiling on anything but VS2015
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include <gdiplus.h>
#include "Screenshot.h"

#pragma comment(lib, "gdiplus.lib")

using namespace std;
using namespace Gdiplus;


/*Screenshot: Default constructer only*/
Screenshot::Screenshot()
{
}

/*~Screenshot: Default deconstructor*/
Screenshot::~Screenshot()
{
}

/*GetEncoderClsid: Image codec creation */
int Screenshot::GetEncoderClsid(const WCHAR* format, CLSID* pClsid) {
	UINT  num = 0;
	UINT  size = 0;

	ImageCodecInfo* pImageCodecInfo = NULL;

	GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;

	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if (pImageCodecInfo == NULL)
		return -1;

	GetImageEncoders(num, size, pImageCodecInfo);
	for (UINT j = 0; j < num; ++j)
	{
		if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;
		}
	}
	free(pImageCodecInfo);
	return 0;
}

/* TakeScreenshot: Uses GDI Plus to capture screen resolution and screen data,
creates bitmap in memory and stored until SaveScreenshot is called

Note: I have not modularized this code on purpose, as the creation of large HBITMAP objects
between functions seems to slow screenshot processing*/
void Screenshot::TakeScreenshot(string userAction, string winName, long xMousePos, long yMousePos) {
	GdiplusStartupInput gdiplusStartupInput;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	HWND hwnd = GetDesktopWindow();
	dc = ::GetDC(0);
	int scaleHeight, scaleWidth = 0;		//Scales resolution to fit user input text at bottom of screen
	int Height = GetSystemMetrics(SM_CYVIRTUALSCREEN);
	int Width = GetSystemMetrics(SM_CXVIRTUALSCREEN);
	scaleHeight = Height + (0.1 * Height);
	memdc = CreateCompatibleDC(dc);
	membit = CreateCompatibleBitmap(dc, Width, scaleHeight);
	HBITMAP bmpContainer = (HBITMAP)SelectObject(memdc, membit);
	BitBlt(memdc, 0, 0, Width, Height, dc, 0, 0, SRCCOPY);

	//Capture cursor info and draw icon onto bitmap using passed in coordinates
	CURSORINFO cursor = { sizeof(cursor) };
	GetCursorInfo(&cursor);
	if (cursor.flags == CURSOR_SHOWING) {
		RECT rect;
		GetWindowRect(hwnd, &rect);
		ICONINFO info = { sizeof(info) };
		GetIconInfo(cursor.hCursor, &info);
		const int x = xMousePos - rect.left - rect.left - info.xHotspot;
		const int y = yMousePos - rect.top - rect.top - info.yHotspot;
		BITMAP bmpCursor = { 0 };
		GetObject(info.hbmColor, sizeof(bmpCursor), &bmpCursor);
		DrawIconEx(memdc, x, y, cursor.hCursor, bmpCursor.bmWidth, bmpCursor.bmHeight,
			0, NULL, DI_NORMAL);
	}
	
	//Draw mouse highlight for old man company owner that can't find cursors easily
	Gdiplus::Graphics gf(memdc);
	Gdiplus::Pen pen(Gdiplus::Color(255,0,255,0));
	pen.SetWidth(4.0F);
	gf.DrawEllipse(&pen, xMousePos-10, yMousePos-10, 40, 40);

	//User input, conversion to wide string
	string text = "User " + userAction + " in " + winName;
	//wstring widestr = std::wstring(text.begin(), text.end());
	const char *widecstr = text.c_str();

	//Create font for text to screenshot
	long lfHeight;
	fontdc = GetDC(NULL);
	lfHeight = -MulDiv(20, GetDeviceCaps(fontdc, LOGPIXELSY), 72);
	ReleaseDC(NULL, fontdc);

	//Add text to screenshot
	HFONT font = CreateFont(lfHeight, 0, 0, 0, 0, FALSE, 0, 0, 0, 0, 0, 0, 0, "Segoe UI");
	SetTextColor(memdc, RGB(255, 255, 255));  //White
	SetBkMode(memdc, TRANSPARENT);
	HFONT previousFont = (HFONT)SelectObject(memdc, font);
	scaleWidth = 0.1 * Width;
	scaleHeight = Height + ((0.1 * Height) / 3);
	TextOut(memdc, scaleWidth, scaleHeight, widecstr, text.size());

	bmpPtr = ::new Bitmap(membit, NULL);
	GetEncoderClsid(L"image/jpeg", &clsid);
}

/* SaveScreenshot: takes in a filename, converts it to 16bit (in windows) wide string, and saves the bitmap to jpg*/
void Screenshot::SaveScreenshot(string filename)
{
	try {
		if (SetCurrentDirectory("C:\\Program Files (x86)\\APC Button\\temp")) { }
		else
		{
			throw ("Unable to set directory to install temp folder");
		}
	}
	catch (exception e) {
		MessageBox(NULL, e.what(), _T("Error"), MB_ICONERROR);
	}

	//Convert filename to wide string 
	wstring widestr = std::wstring(filename.begin(), filename.end());
	const wchar_t* widecstr = widestr.c_str();

	//Save jpeg file
	bmpPtr->Save(widecstr, &clsid, NULL);
	memoryManagement();
}


void Screenshot::memoryManagement()
{
	::delete bmpPtr;
	ReleaseDC(NULL, memdc);
	DeleteObject(fontdc);
	DeleteObject(memdc);
	DeleteObject(membit);
	GdiplusShutdown(gdiplusToken);
}




